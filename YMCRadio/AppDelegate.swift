//
//  YMCRadio
//
//  Created by Mikhail Baynov on 17/10/2016.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//
import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

   var window: UIWindow?


   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
      window = UIWindow(frame: UIScreen.main.bounds)
      window!.rootViewController = MainVC()
      window!.makeKeyAndVisible()
      return true
   }



}

